package com.android.longan;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class MainActivity extends Activity {

	public GlobalVariables global;
	private ProgressDialog pDialogStatus;
	private JSONParser jParser = new JSONParser();

	private String url = "";
	private String url_get_status = "";
	private String url_get_spad = "";
	private String url_update_spad = "";
	private Button button_take_camera;
	private Button button_selectpic;
	private Button btn_submit;
	private ImageView imageview;
	private int serverResponseCode = 0;
	private ProgressDialog dialog = null;
	private ProgressDialog pDialog;
	private String upLoadServerUri = null;
	private String imagepath = null;
	private TextView messageText;
	private float tmpspad_code ;
	private EditText txtLat;
	private EditText txtLng;
	private EditText txtDetail;
	
	
	private String filename;
	private String ids;
	private String imei;
	private String status;
	private String spad_code;
	private String status_txt;
	public static final int REQUEST_CAMERA = 2;
	JSONArray arr = null;
	private Uri uri;

	Timer timer;
	MyTimerTask myTimerTask;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		final Button btn1 = (Button) findViewById(R.id.btn2_submit);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		String method = "create_transaction.php";
		String method_get_status = "get_status.php";
		String method_get_spad = "get_spad.php";
		String method_update_spad = "spad.php";
		
		GlobalVariables gvar = ((GlobalVariables) getApplicationContext());
		url = gvar.getWS_URL() + method;
		url_get_status = gvar.getWS_URL() + method_get_status;
		url_get_spad = gvar.getWS_URL() + method_get_spad;
		url_update_spad = gvar.getWS_URL() + method_update_spad ;
		upLoadServerUri = gvar.getWS_URL() + "UploadToServer.php";
		TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		imei = telephonyManager.getDeviceId();
		blidWidget();
		setActivity();
		initComponent();
		
		btn1.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	            	// Open About
	            	Intent newActivity = new Intent(MainActivity.this,About.class);
	            	startActivity(newActivity);
	        
	            }
	     });

	}
	

	
	

	public void blidWidget() {
//		private EditText txtLat;
//		private EditText txtLng;
//		private EditText txtDetail;
		
		button_take_camera = (Button) findViewById(R.id.button_take_camera);
		button_selectpic = (Button) findViewById(R.id.button_selectpic);
		messageText = (TextView) findViewById(R.id.messageText);
		
		txtLat = (EditText) findViewById(R.id.txtLat);
		txtLng = (EditText) findViewById(R.id.txtLng);
		txtDetail = (EditText) findViewById(R.id.txtDetail);
		
		btn_submit = (Button) findViewById(R.id.btn_submit);
		imageview = (ImageView) findViewById(R.id.imageView_pic);

	}

	public void setActivity() {

		button_take_camera.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
						.format(new Date());
				String imageFileName = "IMG_" + timeStamp + ".jpg";
				File f = new File(Environment.getExternalStorageDirectory(),
						"DCIM/Camera/" + imageFileName);
				uri = Uri.fromFile(f);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
				startActivityForResult(
						Intent.createChooser(intent, "Take a picture with"),
						REQUEST_CAMERA);
			}

		});

		button_selectpic.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(
						Intent.createChooser(intent, "Complete action using"),
						1);
			}

		});

		btn_submit.setOnClickListener(new Button.OnClickListener() {


			@Override
			public void onClick(View v) {

				String Main_String = imagepath;
				String[] a = Main_String.split("/");
				filename = a[a.length - 1];
				new LoadAllAlert().execute();

			}

		});

	}

	public void initComponent() {
		// TODO Auto-generated method stub
		global = ((GlobalVariables) getApplicationContext());
		// Acquire a reference to the system Location Manager
		LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

		// Define a listener that responds to location updates
		LocationListener locationListener = new LocationListener() {
			@Override
		    public void onLocationChanged(Location location) {
		      // Called when a new location is found by the network location provider.

//				private EditText txtLat;
//				private EditText txtLng;
//				private EditText txtDetail;
				
				txtLat.setText(location.getLatitude()+"");
				txtLng.setText(location.getLongitude()+"");
				
				
		    }

		    public void onStatusChanged(String provider, int status, Bundle extras) {}

		    public void onProviderEnabled(String provider) {}

		    public void onProviderDisabled(String provider) {}

		  };

		// Register the listener with the Location Manager to receive location updates
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
	}

	@SuppressWarnings("deprecation")
	public void onBackPressed() {

		AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
				.create();

		// Setting Dialog Title
		alertDialog.setTitle("Exit!");

		// Setting Dialog Message
		alertDialog.setMessage("Are you sure?");

		// Setting OK Button

		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {

				finish();
			}
		});

		alertDialog.setButton2("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				// Write your code here to execute after dialog
				// closed

			}
		});

		// Showing Alert Message
		alertDialog.show();

	}

	@SuppressWarnings("deprecation")
	public void dialogRequire() {

		// AlertDialog alertDialog = new
		// AlertDialog.Builder(PaymentActivity.this)
		// .create();
		//
		// // Setting Dialog Title
		// alertDialog.setTitle("Information!");
		//
		// // Setting Dialog Message
		// alertDialog.setMessage("Please fill in all required information. ");
		//
		// // Setting OK Button
		//
		// alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		// public void onClick(DialogInterface dialog, int which) {
		// // Write your code here to execute after dialog
		// // closed
		//
		// // Intent i = new Intent(Accel.this,
		// // MenuActivity.class);
		// // startActivity(i);
		//
		// }
		// });
		//
		// // Showing Alert Message
		// alertDialog.show();
	}

	/**
	 * Background Async Task to Load all Alert by making HTTP Request
	 * */
	class LoadAllAlert extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(MainActivity.this);
			pDialog.setMessage("Loading alert. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All alerts from url
		 * */
		protected String doInBackground(String... args) {

			new Thread(new Runnable() {
				public void run() {

					uploadFile(imagepath);

				}
			}).start();

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("imei", imei));
			params.add(new BasicNameValuePair("img_name", filename));
			params.add(new BasicNameValuePair("lat", txtLat.getText().toString().trim()));
			params.add(new BasicNameValuePair("lng", txtLng.getText().toString().trim()));
			params.add(new BasicNameValuePair("desc", txtDetail.getText().toString().trim()));

			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(url, "GET", params);

			// Check your log cat for JSON reponse
			Log.d("All Alert: ", json.toString());

			try {
				// Checking for SUCCESS TAG
				int success = json.getInt("success");
				if (success == 1) {

					ids = json.getString("transaction_id").toString();
					// user_id = json.getString("user_id");
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							Toast.makeText(MainActivity.this,
									"Successfully id : " + ids,
									Toast.LENGTH_SHORT).show();
							if (timer != null) {
								timer.cancel();
							}
							List<NameValuePair> params = new ArrayList<NameValuePair>();
							params.add(new BasicNameValuePair("ids", ids));

							// getting JSON string from URL
							JSONObject json = jParser.makeHttpRequest(url_update_spad, "GET",
									params);
							// re-schedule timer here
							// otherwise, IllegalStateException of
							// "TimerTask is scheduled already"
							// will be thrown
							timer = new Timer();
							myTimerTask = new MyTimerTask();

							// delay 1000ms, repeat in 5000ms
							timer.schedule(myTimerTask, 1000, 5000);

							pDialogStatus = new ProgressDialog(
									MainActivity.this);
							pDialogStatus
									.setMessage("Processing. Please wait...");
							pDialogStatus.setIndeterminate(false);
							pDialogStatus.setCancelable(false);
							pDialogStatus.show();
						}
					});

				} else {
					// no alert found

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all alert
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {

				}
			});

		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == 1 && resultCode == RESULT_OK) {
			// Bitmap photo = (Bitmap) data.getData().getPath();

			Uri selectedImageUri = data.getData();
			imagepath = getPath(selectedImageUri);
			Bitmap bitmap = BitmapFactory.decodeFile(imagepath);
			imageview.setImageBitmap(bitmap);
			messageText.setText("Uploading file path:" + imagepath);

		} else if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {
			getContentResolver().notifyChange(uri, null);
			ContentResolver cr = getContentResolver();
			try {
				Bitmap bitmap = Media.getBitmap(cr, uri);
				imageview.setImageBitmap(bitmap);
				Toast.makeText(getApplicationContext(), uri.getPath(),
						Toast.LENGTH_SHORT).show();

				imagepath = uri.getPath();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	public int uploadFile(String sourceFileUri) {

		String fileName = sourceFileUri;

		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		File sourceFile = new File(sourceFileUri);

		if (!sourceFile.isFile()) {

			dialog.dismiss();

			Log.e("uploadFile", "Source File not exist :" + imagepath);

			runOnUiThread(new Runnable() {
				public void run() {
					messageText.setText("Source File not exist :" + imagepath);
				}
			});

			return 0;

		} else {
			try {

				// open a URL connection to the Servlet
				FileInputStream fileInputStream = new FileInputStream(
						sourceFile);
				URL url = new URL(upLoadServerUri);

				// Open a HTTP connection to the URL
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true); // Allow Inputs
				conn.setDoOutput(true); // Allow Outputs
				conn.setUseCaches(false); // Don't use a Cached Copy
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn.setRequestProperty("Content-Type",
						"multipart/form-data;boundary=" + boundary);
				conn.setRequestProperty("uploaded_file", fileName);

				dos = new DataOutputStream(conn.getOutputStream());

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
						+ fileName + "\"" + lineEnd);

				dos.writeBytes(lineEnd);

				// create a buffer of maximum size
				bytesAvailable = fileInputStream.available();

				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				while (bytesRead > 0) {

					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				}

				// send multipart form data necesssary after file data...
				dos.writeBytes(lineEnd);
				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

				// Responses from the server (code and message)
				serverResponseCode = conn.getResponseCode();
				String serverResponseMessage = conn.getResponseMessage();

				Log.i("uploadFile", "HTTP Response is : "
						+ serverResponseMessage + ": " + serverResponseCode);

				if (serverResponseCode == 200) {

					runOnUiThread(new Runnable() {
						public void run() {
							// String msg =
							// "File Upload Completed.\n\n See uploaded file here : \n\n"
							// + " F:/wamp/wamp/www/uploads";
							String msg = "File Upload Completed.";
							messageText.setText(msg);
							Toast.makeText(MainActivity.this,
									"File Upload Complete.", Toast.LENGTH_SHORT)
									.show();
						}
					});
				}

				// close the streams //
				fileInputStream.close();
				dos.flush();
				dos.close();

			} catch (MalformedURLException ex) {

				dialog.dismiss();
				ex.printStackTrace();

				runOnUiThread(new Runnable() {
					public void run() {
						messageText
								.setText("MalformedURLException Exception : check script url.");
						Toast.makeText(MainActivity.this,
								"MalformedURLException", Toast.LENGTH_SHORT)
								.show();
					}
				});

				Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
			} catch (Exception e) {

				dialog.dismiss();
				e.printStackTrace();

				runOnUiThread(new Runnable() {
					public void run() {
						messageText.setText("Got Exception : see logcat ");
						Toast.makeText(MainActivity.this,
								"Got Exception : see logcat ",
								Toast.LENGTH_SHORT).show();
					}
				});
				Log.e("Upload file to server Exception",
						"Exception : " + e.getMessage(), e);
			}
			// dialog.dismiss();
			return serverResponseCode;

		} // End else block
	}

	class MyTimerTask extends TimerTask {

		@Override
		public void run() {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("ids", ids));

			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(url_get_status, "GET",
					params);

			// Check your log cat for JSON reponse
			Log.d("All Alert: ", json.toString());

			try {
				// Checking for SUCCESS TAG
				int success = json.getInt("success");

				if (success == 1) {
					status = json.getString("status").toString();
					spad_code = json.getString("spad_code").toString();
					runOnUiThread(new Runnable() {
						public void run() {
							Toast.makeText(MainActivity.this, status,
									Toast.LENGTH_SHORT).show();

							if (status.equals("1")) {
								
								

								if (timer != null) {
									timer.cancel();
									timer = null;
									// pDialogStatus.dismiss();
									getspad();
								}
							}
						}
					});

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// textCounter.setText(strDate);

				}
			});
		}

	}

	public void getspad() {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("ids", ids));

		// getting JSON string from URL
		JSONObject json = jParser.makeHttpRequest(url_get_spad, "GET", params);

		// Check your log cat for JSON reponse
		Log.d("All Alert: ", json.toString());

		try {
			// Checking for SUCCESS TAG
			int success = json.getInt("success");

			if (success == 1) {
				arr = json.getJSONArray("array");
				tmpspad_code = 0 ;
				// looping through All Alert
				for (int i = 0; i < arr.length(); i++) {
					JSONObject c = arr.getJSONObject(i);
//					int tmpspad_code = Integer.parseInt(spad_code);
//				    int tmpFirst = Integer.parseInt(c.getString("spad_first"));
//					int tmpLast = Integer.parseInt(c.getString("spad_last"));
					
					tmpspad_code = Float.parseFloat(spad_code);
					float tmpFirst = Float.parseFloat(c.getString("spad_first"));
					float tmpLast = Float.parseFloat(c.getString("spad_last"));
					
					Log.i("----------tmpID-------------", tmpspad_code+"");
					Log.i("----------tmpFirst-------------", tmpFirst+"");
					Log.i("----------tmpLast-------------", tmpLast+"");
					Log.i("----------status_txt-------------", status_txt+"");

					if (tmpspad_code >= tmpFirst && tmpspad_code <= tmpLast) {

						status_txt = c.getString("spad_detail");
						

					}

				}

				// ---------

				runOnUiThread(new Runnable() {
					@SuppressWarnings("deprecation")
					public void run() {
						// Toast.makeText(MainActivity.this, status,
						// Toast.LENGTH_SHORT).show();
						pDialogStatus.dismiss();
						AlertDialog alertDialog = new AlertDialog.Builder(
								MainActivity.this).create();

						// Setting Dialog Title
						alertDialog.setTitle("SPAD : "+ tmpspad_code);

						// Setting Dialog Message
						alertDialog.setMessage("คำแนะนำ :" + status_txt);

						// Setting OK Button

						alertDialog.setButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										dialog.cancel();
									}
								});

						alertDialog.show();

					}
				});

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
}
