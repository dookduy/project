package com.android.longan;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;

public class SplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_layout);

		int secondsDelayed = 2;
		new Handler().postDelayed(new Runnable() {
			public void run() {
				nextPage();
			}
		}, secondsDelayed * 2000);
	}

	private void nextPage() {

		Intent i = new Intent(SplashActivity.this, MainActivity.class);
		startActivity(i);
		finish();
	}

}
