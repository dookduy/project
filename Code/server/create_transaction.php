<?php

/*
 * Following code will create a new product row
 * All product details are read from HTTP Post Request
 */

// array for JSON response
$response = array();

// check for required fields
if (isset($_GET['imei'])&& ($_GET['img_name']) && ($_GET['lat']) && ($_GET['lng']) && ($_GET['desc']) ) {
    
    $imei = $_GET['imei'];
	$img_name = $_GET['img_name'];
    $lat = $_GET['lat'];
	$lng = $_GET['lng'];
	$desc = $_GET['desc'];

    // include db connect class
    require_once __DIR__ . '/db_connect_alert.php';

    // connecting to db
    $db = new DB_CONNECT();

    // mysql inserting a new row
    $result = mysql_query("INSERT INTO transaction(imei,img_name,lat,lng,description) VALUES('$imei', '$img_name','$lat','$lng','$desc')");

    // check if row inserted or not
    if ($result) {
        // successfully inserted into database
		$result1 = mysql_query("SELECT MAX(transaction_id) AS transaction_id FROM transaction;");
		$row = mysql_fetch_array($result1);
		$response["transaction_id"] = $row["transaction_id"];
        $response["success"] = 1;
        $response["message"] = "Product successfully created.";

        // echoing JSON response
        echo json_encode($response);
    } else {
        // failed to insert row
        $response["success"] = 0;
        $response["message"] = "Oops! An error occurred.";
        
        // echoing JSON response
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>