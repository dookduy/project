import numpy as np
import cv2
import sys
import colorsys

def rgb2spad(r,g,b):
    spad = 64.28+(-0.9464*r)+(0.44444*g)-(0.02606*b)
    return abs(spad)    


def avg(input_):
    res1 = tuple(input_)
    arr = np.array(res1) 
    arrrgb = arr.reshape(arr.size/3,3)
    darr = arrrgb[~np.all(arrrgb==0,axis = 1 )] 
    avgRGB = np.average(darr,axis = 0)
    return avgRGB

img = cv2.imread(sys.argv[1])
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
ret, thresh = cv2.threshold(gray,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
res = cv2.bitwise_and(img,img, mask= thresh )
avgrgb = avg(res)
avgspad = rgb2spad(avgrgb[2],avgrgb[1],avgrgb[0])

print  avgspad